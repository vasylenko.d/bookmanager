# Spring MVC, Hibernate, MySQL training project

#### For cloning repo by 'https', please enter to terminal:
> $git clone https://gitlab.com/vasylenko.d/bookmanager.git

#### For MySQL database:
- cloning MySQL docker-compose.yml, please enter from your project directory:
> $wget https://gitlab.com/vasylenko.d/docker/blob/master/mysql/docker-compose.yml

- from your project directory, start up your application by running:
> $docker-compose up

- script for create table 'BOOKS', please enter to terminal:
> $wget https://gitlab.com/vasylenko.d/sql/blob/master/src/mysql/bookmanager/ddl.sql

#### This example covers the following:
- CRUD (Create, Read, Update and Delete) application
- 100% Java-configured project(migrate the Servlet declaration from the web.xml 
        file to Java config)

## Version
- Spring 5.1.9
- Hibernate 5.4.4
- Servlet-api 4.0.1
- Slf4j 1.7.28
