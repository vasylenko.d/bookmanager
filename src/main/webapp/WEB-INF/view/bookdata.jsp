<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
  <title>BookData</title>
  <link href="../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<c:url value="/books"/>" target="_blank">Back to books menu</a>
<br/>
<br/>

<table class="tg">
  <caption>Book Details</caption>
  <tr>
    <th scope="col" class="th_80">ID</th>
    <th scope="col" class="th_120">Title</th>
    <th scope="col" class="th_120">Author</th>
    <th scope="col" class="th_120">ISBN</th>
    <th scope="col" class="th_120">Price</th>
  </tr>
  <tr>
    <td>${book.id}</td>
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td>${book.isbn}</td>
    <td>${book.price/100}</td>
  </tr>
</table>
</body>
</html>