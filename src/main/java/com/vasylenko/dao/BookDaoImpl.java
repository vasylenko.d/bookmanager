package com.vasylenko.dao;

import com.vasylenko.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class BookDaoImpl implements BookDao
{
    private static final Logger LOG = LoggerFactory.getLogger(BookDaoImpl.class);
    private static final String SQL_QUERY_FROM_BOOKS = "from Book";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addBook(Book book)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(book);

        LOG.info("Book successfully saved. Book details: {}", book);
    }

    @Override
    public void updateBook(Book book)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(book);

        LOG.info("Book successfully update. Book details: {}", book);
    }

    @Override
    public void removeBook(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = session.load(Book.class, id);

        if (Objects.nonNull(book))
        {
            session.delete(book);
        }

        LOG.info("Book successfully delete.");
    }

    @Override
    public Book getBookById(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = session.load(Book.class, id);

        LOG.info("Book successfully loaded. Book details: {}", book);

        return book;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> getAllBooks()
    {
        Session session = this.sessionFactory.getCurrentSession();
        List<Book> bookList = session.createQuery(SQL_QUERY_FROM_BOOKS).list();

        for (Book book : bookList)
        {
            LOG.info("Book list: {}", book);
        }

        return bookList;
    }
}
