package com.vasylenko.service;

import com.vasylenko.dao.BookDao;
import com.vasylenko.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService
{
    @Autowired
    @Qualifier(value = "bookDao")
    private BookDao dao;

    @Override
    @Transactional
    public void addBook(Book book)
    {
        this.dao.addBook(book);
    }

    @Override
    @Transactional
    public void updateBook(Book book)
    {
        this.dao.updateBook(book);
    }

    @Override
    @Transactional
    public void removeBook(int id)
    {
        this.dao.removeBook(id);
    }

    @Override
    @Transactional
    public Book getBookById(int id)
    {
        return this.dao.getBookById(id);
    }

    @Override
    @Transactional
    public List<Book> getAllBooks()
    {
        return this.dao.getAllBooks();
    }
}
